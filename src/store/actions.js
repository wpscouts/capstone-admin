// Import Libraries
import axios from 'axios'

// Import Router
import router from '@/router'

// Define Actions
export const actionsIndex = {
  // Loader Action(s)
  async loaderStatus ({state, getters, commit}, payload) {
    commit('siteLoader', payload)
  },

  // Error Action(s)
  async clearErrors ({state, getters, commit}) {
    commit('errors')
  },

  // Themes Action(s)
  async getThemes ({state, getters, commit}) {
    try {
      const response = await axios.get('/settings/themes')
      commit('themes', response.data)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },
  async updateThemes ({state, getters, commit}, payload) {
    try {
      const response = await axios.put('/settings/themes', { payload })
      commit('themes', response.data)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },

  // Setting Action(s)
  async getSettings ({state, getters, commit}) {
    const response = await axios.get('/settings')
    commit('settings', response.data)
  },
  async updateSettings ({state, getters, commit}, payload) {
    try {
      const response = await axios.put('/settings', { payload })
      commit('settings', response.data)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },

  // Subscription Action(s)
  async getSubscription ({state, getters, commit}) {
    try {
      const response = await axios.get('/customers/subscription')
      commit('subscription', response.data.subscription)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },
  async deleteSubscription ({state, getters, commit}) {
    try {
      await axios.delete('/customers/subscription')
      commit('subscription', '')
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },

  // Card Action(s)
  async getCard ({state, getters, commit}) {
    try {
      const response = await axios.get('/customers/card')
      commit('card', response.data.card)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },
  async deleteCard ({state, getters, commit}) {
    try {
      await axios.delete('/customers/card')
      commit('card', '')
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },

  // Profile Action(s)
  async getProfile ({state, getters, commit}) {
    const response = await axios.get('/users/profile')
    commit('profile', response.data)
  },
  async updateProfile ({state, getters, commit}, payload) {
    try {
      const response = await axios.put('/users/update/', { payload })
      commit('profile', response.data)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },

  // Authorization Action(s)
  async getAuthorizations ({state, getters, commit}) {
    const response = await axios.get('/authorization')
    commit('authorizations', response.data)
  },
  async updateAuthorizations ({state, getters, commit}, payload) {
    try {
      const response = await axios.put('/authorization', { action: payload })
      commit('authorizations', response.data.data)
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },

  // Authentication Action(s)
  async authenticateLogin ({state, getters, commit}, payload) {
    try {
      const response = await axios.post('/authentication/login', { payload })
      localStorage.setItem('authToken', response.data.token) // set token in localStorage
      commit('isAuthenticated', true) // set local authentication state
      router.push({ name: 'admin' }) // redirect user to admin panel
    } catch (error) {
      commit('errors', error.response.data.message)
    }
  },
  async authenticateToken ({state, getters, commit}, payload) {
    commit('isAuthenticated', payload)
  },
  async authenticateSubscription ({state, getters, commit}, payload) {
    commit('subscription', payload)
  }
}
