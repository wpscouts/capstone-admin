// Import Libraries
import Vue from 'vue'
import Vuex from 'vuex'

// Import Members
import { gettersIndex } from './getters'
import { mutationsIndex } from './mutations'
import { actionsIndex } from './actions'

// Register Vuex
Vue.use(Vuex)

// Configure Vuex Store
const store = new Vuex.Store({
  state: {
    baseAPI: 'http://lvh.me:9000/api/v1',
    siteLoader: false,
    isAuthenticated: false,
    errors: [],
    profile: {},
    settings: {},
    themes: [],
    subscription: {},
    card: {},
    authorizations: {}
  },
  getters: Object.assign({}, gettersIndex),
  mutations: Object.assign({}, mutationsIndex),
  actions: Object.assign({}, actionsIndex)
})

// Export Store
export default store
