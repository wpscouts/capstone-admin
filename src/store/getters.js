export const gettersIndex = {
  // Get base URL for API
  baseAPI: (state, getters) => {
    return state.baseAPI
  },
  // Get site loader status
  siteLoader: (state, getters) => {
    return state.siteLoader
  },
  // Get authentication status
  isAuthenticated: (state, getters) => {
    return state.isAuthenticated
  },
  // Get current stack of errors
  errors: (state, getters) => {
    return state.errors
  },
  // Get user profile
  profile: (state, getters) => {
    return state.profile
  },
  // Get user settings
  settings: (state, getters) => {
    return state.settings
  },
  // Get user themes
  themes: (state, getters) => {
    return state.themes
  },
  // Get user subscription
  subscription: (state, getters) => {
    return state.subscription
  },
  // Get user card
  card: (state, getters) => {
    return state.card
  },
  // Get user authorizations
  authorizations: (state, getters) => {
    return state.authorizations
  }
}
