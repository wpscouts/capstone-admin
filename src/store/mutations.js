export const mutationsIndex = {
  siteLoader (state, payload) {
    state.siteLoader = payload // set 'siteLoader' state with payload
  },
  isAuthenticated (state, payload) {
    state.isAuthenticated = payload
  },
  errors (state, payload) {
    state.errors = [] // clear previous 'errors' state
    if (payload) state.errors.push(payload) // push new 'error(s)' to local state
  },
  profile (state, payload) {
    state.profile = payload // Updates state profile
  },
  settings (state, payload) {
    state.settings = payload // Updates state settings
  },
  themes (state, payload) {
    state.themes = payload // Updates state themes
  },
  subscription (state, payload) {
    state.subscription = payload // Updates state subscription
  },
  card (state, payload) {
    state.card = payload // Updates state card
  },
  authorizations (state, payload) {
    state.authorizations = payload // Updates state authorizations
  }
}
