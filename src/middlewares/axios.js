// Import Libraries
import axios from 'axios'

// AXIOS: Add a request interceptor
axios.interceptors.request.use(function (config) {
  // Do something before request is sent
  router.app.$Progress.start()
  return config
}, function (error) {
  // Do something with request error
  router.app.$Progress.fail()
  return Promise.reject(error)
})

// AXIOS: Add a response interceptor
axios.interceptors.response.use(function (response) {
  // Do something with response data
  router.app.$Progress.finish()
  return response
}, function (error) {
  // Do something with response error
  router.app.$Progress.fail()
  return Promise.reject(error)
})