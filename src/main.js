// Import Libraries
import Vue from 'vue'
import axios from 'axios'
import VueProgressBar from 'vue-progressbar'
import * as svgicon from 'vue-svgicon'

// Import Router
import router from './router'

// Import Store
import store from './store'

// Import Components
import App from './App'

// App Level Middlewares
Vue.use(VueProgressBar)
Vue.use(svgicon, {tagName: 'svgicon'})

// AXIOS: Add a request interceptor
axios.interceptors.request.use(function (config) {
  // Do something before request is sent
  store.dispatch('loaderStatus', true) // Set loader status to true
  router.app.$Progress.start() // Start progress bar at the top
  return config
}, function (error) {
  // Do something with request error
  store.dispatch('loaderStatus', false)// Set loader status to false
  router.app.$Progress.fail() // Finishes progress bar with error
  return Promise.reject(error)
})

// AXIOS: Add a response interceptor
axios.interceptors.response.use(function (response) {
  // Do something with response data
  store.dispatch('loaderStatus', false) // Set loader status to false
  router.app.$Progress.finish() // Finishes progress bar
  return response
}, function (error) {
  // Do something with response error
  store.dispatch('loaderStatus', false) // Set loader status to false
  router.app.$Progress.fail() // Finishes progress bar with error
  return Promise.reject(error)
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
