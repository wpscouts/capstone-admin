// Import Components
import Login from '@/components/login/Login.vue'
import Register from '@/components/register/Register.vue'

import Admin from '@/components/Admin.vue'

import Projects from '@/components/projects/Projects.vue'
import Pages from '@/components/pages/Pages.vue'
import Themes from '@/components/themes/Themes.vue'
import Customize from '@/components/customize/Customize.vue'

import Profile from '@/components/profile/Profile.vue'
import Settings from '@/components/settings/Settings.vue'
import Billing from '@/components/billing/Billing.vue'
import Usage from '@/components/usage/Usage.vue'

import Error404 from '@/components/Error404.vue'

// Routes Definition
export default [
  { path: '/', redirect: { name: 'login' } },
  { path: '/login', name: 'login', component: Login },
  { path: '/logout', name: 'logout', meta: { removesAuth: true } },
  { path: '/register', name: 'register', component: Register },

  { path: '/admin',
    name: 'admin',
    component: Admin, // admin scaffolding
    meta: { requiresAuth: true },
    children: [
      { path: 'projects', name: 'projects', component: Projects, meta: { title: 'Projects' } },
      { path: 'pages', name: 'pages', component: Pages, meta: { title: 'Pages' } },
      { path: 'themes', name: 'themes', component: Themes, meta: { title: 'Themes' } },
      { path: 'customize', name: 'customize', component: Customize },

      { path: 'profile', name: 'profile', component: Profile, meta: { title: 'Profile' } },
      { path: 'settings', name: 'settings', component: Settings, meta: { title: 'Settings' } },
      { path: 'billing', name: 'billing', component: Billing, meta: { title: 'Billing' } },
      { path: 'usage', name: 'usage', component: Usage, meta: { title: 'Usage Statistics' } }
    ]
  },

  { path: '*', name: 'error', component: Error404 }
]
