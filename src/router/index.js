// Import Modules
import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'

// Import Routes Definition
import Routes from './routes'

// Import Store
import store from '@/store'

// Register Routes with Vue app
Vue.use(Router)

// Router Configuration
const router = new Router({
  mode: 'history',
  routes: Routes
})

// Navigation Guards (global)

/**
 * BEFORE EACH ROUTE
 *
 * Clears all errors in local state
 * Set axios default URL and TOKEN
 *
 */
router.beforeEach((to, from, next) => {
  // clear 'errors' state before changing route
  store.dispatch('clearErrors')

  // Set 'axios' globals before any axios request is sent
  axios.defaults.baseURL = store.getters.baseAPI
  axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('authToken')}`

  next()
})

/**
 * BEFORE EACH ROUTE
 *
 * Check if they requires authentication (protected routes)
 * Check authentication via local state and local storage
 *
 */
router.beforeEach(async (to, from, next) => {
  const protectedRoute = to.matched.some(record => record.meta.requiresAuth)

  if (protectedRoute) {
    try {
      console.log('Checking if route is authorised ...')

      // Check user authentication status in local state
      const isAuthenticated = store.getters.isAuthenticated

      // If above is false, Verify user authentication status from server based on token in localStorage
      if (!isAuthenticated) {
        await axios.post('/authentication/token') // verify token from server
        store.dispatch('authenticateToken', true) // if verified also update local authentication state
      }

      console.log('Route is authorised!')
    } catch (error) {
      // If token is not present in localStorage OR the token present is not valid
      console.log('Route is not authorised!')
      return next({ name: 'login' })
    }
  }

  next()
})

/**
 * BEFORE EACH ROUTE
 *
 * Check if their subscription is valid (protected routes)
 * Check subscription via local state or database
 *
 */
router.beforeEach(async (to, from, next) => {
  const nonBillingRoute = to.matched.some(record => record.meta.requiresAuth) && to.name !== 'billing'

  if (nonBillingRoute) {
    try {
      console.log('Checking if has valid subscription ...')

      // Check user authentication status in local state
      const hasSubscription = Object.keys(store.getters.subscription).length !== 0

      // If above is false, Verify user authentication status from server based on token in localStorage
      if (!hasSubscription) {
        const response = await axios.get('/customers/subscription') // verify token from server
        store.dispatch('authenticateSubscription', response.data.subscription) // if verified also update local authentication state
      }

      console.log('Subscription is valid!')
    } catch (error) {
      // If token is not present in localStorage OR the token present is not valid
      console.log('Subscription is not valid!')
      return next({ name: 'billing' })
    }
  }

  next()
})

/**
 * BEFORE EACH ROUTE
 *
 * Check if they are accessing 'Logout' route
 * If they are remove all traces from local state and local storage
 *
 */
router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.removesAuth)) {
    console.log('Going to Logout ...')

    store.dispatch('authenticateToken', false) // remove authetication status from local state
    localStorage.removeItem('authToken') // remove authetication token from localStorage

    console.log('Logged out and removed traces.')

    return next({ name: 'login' })
  }

  next()
})

/**
 * BEFORE EACH ROUTE
 *
 * Check if they are accessing 'Login' or 'Register' routes
 * If they are and they are logged-in redirect them to admin-panel
 *
 */
router.beforeEach(async (to, from, next) => {
  if (to.name === 'login' || to.name === 'register') {
    console.log('Accessing a non-protected route.')

    // check if user is already logged-in
    const isAuthToken = localStorage.getItem('authToken')

    // if is autheticated and is trying to reach 'login' or 'register' route, reirect to admin
    if (isAuthToken) {
      return next({ name: 'admin' })
    }
  }

  next()
})

// Export Router
export default router
